import Mock from 'mockjs'

Mock.mock('/dev-api/user/login', 'post', {
    code: 0,
    data: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InNhIiwidXNlcklkIjoxLCJyb2xlSWQiOjEsImV4cCI6MTY5MzkwNzI5NSwiaXNzIjoiand0LWlzc3VlciJ9.wMY1cMZgH_8jEMhy7lKlSEJr4dX1zZaPpxEFVSOHYF0',
        username: 'sa',
    },
    msg: '操作成功',
})

Mock.mock('/dev-api/user/userInfo', 'get', {
    code: 0,
    data: {
        has_password: true,
        user_info: {
            id: 1,
            created_at: '2023-08-16T21:33:28.044646+08:00',
            updated_at: '2023-08-16T21:33:28.044646+08:00',
            username: 'sa',
            display_name: 'sa',
            custom_group: '',
            remark: '',
            header_img: '',
            role_id: 1,
            role: {
                id: 1,
                created_at: '2023-08-16T21:16:04.767595+08:00',
                updated_at: '2023-08-25T21:55:09.808056+08:00',
                role_name: 'superAdmin',
                role_value: 777,
                description: '超级管理员',
                sort: 0,
            },
        },
    },
    msg: '操作成功',
})
