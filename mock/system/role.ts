import Mock from 'mockjs'

Mock.mock('/dev-api/role/list', 'get', {
    code: 0,
    data: {
        list: [
            {
                id: 1,
                created_at: '2023-08-16T21:16:04.767595+08:00',
                updated_at: '2023-08-16T21:46:16.082163+08:00',
                role_name: 'superAdmin',
                role_value: 777,
                description: '超级管理员',
                sort: 0,
            },
            {
                id: 2,
                created_at: '2023-08-16T21:16:04.767595+08:00',
                updated_at: '2023-08-16T21:46:16.082163+08:00',
                role_name: 'admin',
                role_value: 555,
                description: '管理员',
                sort: 0,
            },
            {
                id: 3,
                created_at: '2023-08-16T21:16:04.767595+08:00',
                updated_at: '2023-08-16T21:46:16.082163+08:00',
                role_name: 'validUser',
                role_value: 444,
                description: '用户',
                sort: 0,
            },
        ],
        total: 3,
    },
    msg: '操作成功',
})
