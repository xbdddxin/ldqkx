import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import { createPlugins } from './src/plugins/vite'

// https://vitejs.dev/config/
export default defineConfig(async ({ mode, command }) => {
    const env = loadEnv(mode, process.cwd())
    const { VITE_APP_ENV } = env

    return {
        base: VITE_APP_ENV === 'production' ? '/' : '/',
        plugins: await createPlugins({ isBuild: command === 'build' }),
        resolve: {
            alias: {
                '@': fileURLToPath(new URL('./src', import.meta.url)),
                '~': fileURLToPath(new URL('./', import.meta.url)),
            },
        },
        server: {
            port: 8085,
            host: true,
            // open: true,
            proxy: {
                // https://cn.vitejs.dev/config/#server-proxy
                '/dev-api': {
                    target: 'http://excalibur.link:8899/',
                    changeOrigin: true,
                    rewrite: p => p.replace(/^\/dev-api/, ''),
                },
            },
        },
    }
})
