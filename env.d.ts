/// <reference types="vite/client" />

interface ImportMetaEnv {
    readonly VITE_APP_TITLE: string
    readonly VITE_APP_ENV: string
    readonly VITE_APP_BASE_API: string
    readonly VITE_SUPER_ADMIN: number
}

interface ImportMeta {
    readonly env: ImportMetaEnv
}
