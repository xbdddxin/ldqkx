import { createApp } from 'vue'
import { createPinia } from 'pinia'

import ElementPlus from 'element-plus'
import locale from 'element-plus/es/locale/lang/zh-cn' // 中文语言
import 'element-plus/dist/index.css'
// @ts-expect-error moduleResolution:nodenext issue 54523
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import { ElMessage, ElMessageBox } from 'element-plus'

import '@/assets/style/index.scss'

import App from './App.vue'
import router from './router'

import { timeFormatter } from '@/utils/tools'

import { rigistDirective } from '@/directives'

// mockjs
// import '../mock'

// 全局方法声明
declare module 'vue' {
    interface ComponentCustomProperties {
        $message: typeof ElMessage
        $msgbox: typeof ElMessageBox
        $timeFormatter: typeof timeFormatter
    }
}

const app = createApp(App)

// 注册 el-icon
for (const [key, comp] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, comp as Component)
}

// 注册全局方法
app.config.globalProperties.$timeFormatter = timeFormatter

// 注册全局指令
rigistDirective(app)

app.use(createPinia())
app.use(router)
app.use(ElementPlus, { locale: locale })

app.mount('#app')
