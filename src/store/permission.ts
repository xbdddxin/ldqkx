import { ElMessage } from 'element-plus'
import { getMenuList } from '@/api/system/menu'
import { RouteItem } from '@/api/system/model/menuModel'
import { HomePage, publicRoutes } from '@/router'
import DefaultLayout from '@/layouts/default/index.vue'

interface PermissionState {
    // 顶部导航栏菜单
    topbarRoutes?: []
    // 侧边栏菜单路由
    sidebarRoutes: RouteItem[]
    // 后台获取的动态路由
    dynamicRoutes: RouteItem[]
    // vue-router中的所有路由
    routes: RouteItem[]
}

const usePermissionStore = defineStore('permission', {
    state: (): PermissionState => ({
        sidebarRoutes: [],
        dynamicRoutes: [],
        routes: [],
    }),
    actions: {
        buildRoutes(): Promise<RouteItem[] | null> {
            return new Promise(resolve => {
                // 向后端请求路由数据
                getMenuList()
                    .then(res => {
                        let dr: RouteItem[] = []
                        if (res) {
                            const routes: RouteItem[] = JSON.parse(JSON.stringify(res))
                            dr = filterAsyncRouter(routes)
                        }
                        this.dynamicRoutes = dr
                        // dynamicRoutes 加上首页 Homepage 就是 sidebarRoutes
                        const sr = [HomePage as RouteItem].concat(dr)
                        this.sidebarRoutes = sr
                        // sidebarRoutes 加上公共路由 publicRoutes 就是 routes
                        this.routes = (publicRoutes as RouteItem[]).concat(sr)
                        resolve(sr)
                    })
                    .catch(err => {
                        ElMessage.error('菜单获取错误：' + err)
                    })
            })
        },
    },
})

// 过滤路由
function filterAsyncRouter(asyncRouterMap: RouteItem[]): RouteItem[] {
    const filterRoutes: RouteItem[] = []
    asyncRouterMap.forEach(route => {
        if (route.children != null && route.children && route.children.length) {
            route.children = filterAsyncRouter(route.children)
        }

        const routeItem: RouteItem = {
            id: route.id,
            path: route.path,
            component: '',
            name: route.name,
            redirect: route.redirect,
            meta: {
                title: route.meta?.title,
                icon: route.meta?.icon,
            },
            children: route.children,
        }

        // 没有 children 的路由删除 children 和 redirect 属性
        if (!(route.children != null && route.children && route.children.length)) {
            delete routeItem['children']
            delete routeItem['redirect']
        }

        if (route.component) {
            // DefaultLayout 组件特殊处理
            if (route.component === 'DefaultLayout') {
                routeItem.component = DefaultLayout
            } else {
                routeItem.component = loadView(route.component)
            }
        }

        filterRoutes.push(routeItem)
    })
    return filterRoutes
}

// 匹配views里面所有的.vue文件
const modules = import.meta.glob('./../views/**/*.vue')
// 将组件路径 转换为 对应的组件实例
function loadView(view: string) {
    let comp
    for (const path in modules) {
        const dir = path.split('../views/')[1]
        if (dir === view) {
            comp = () => modules[path]()
        }
    }
    return comp
}

export default usePermissionStore
