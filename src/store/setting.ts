interface SettingState {
    // 页面标题
    title: string
    layoutSetting: {
        // 是否开启页面标签
        tagsView: boolean
        // 是否固定头部
        fixedHeader: boolean
    }
}

const useSettingStore = defineStore('setting', {
    state: (): SettingState => ({
        title: '',
        layoutSetting: {
            tagsView: true,
            fixedHeader: true,
        },
    }),
    actions: {},
})

export default useSettingStore
