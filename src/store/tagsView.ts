import type { RouteLocationNormalizedLoaded } from 'vue-router'
import router from '@/router'
import { log } from 'console'

interface TagsViewState {
    // 已打开的页面
    visitedViews: RouteLocationNormalizedLoaded[]
    // 缓存的页面（用于keep-alive）
    cachedViews: string[]
}

const useTagsViewStore = defineStore('tagsView', {
    state: (): TagsViewState => ({
        visitedViews: [],
        cachedViews: [],
    }),
    actions: {
        // 刷新指定tab页签
        refreshPage(view?: RouteLocationNormalizedLoaded) {
            // 如果无传参则刷新当前页面
            if (view === undefined) {
                view = router.currentRoute.value
            }
            this.delCachedView(view).then(() => {
                const { path, query } = view!
                router.replace({
                    path: '/redirect' + path,
                    query: query,
                })
            })
        },
        // 关闭指定tab页签
        closePage(view?: RouteLocationNormalizedLoaded): Promise<{ visitedViews: RouteLocationNormalizedLoaded[]; cachedViews: string[] }> {
            // 如果无传参则关闭当前页面
            if (view === undefined) {
                this.delView(router.currentRoute.value).then(({ visitedViews }) => {
                    const latestView = visitedViews.slice(-1)[0]
                    if (latestView) {
                        return router.push(latestView.fullPath)
                    }
                    return router.push('/')
                })
            }
            return this.delView(view!)
        },
        // 关闭其他tab页签
        closeOtherPage(view: RouteLocationNormalizedLoaded) {
            return this.delOthersViews(view || router.currentRoute.value)
        },
        // 关闭所有tab页签
        closeAllPage() {
            return this.delAllViews()
        },
        addView(view: RouteLocationNormalizedLoaded) {
            this.addVisitedView(view)
            this.addCachedView(view)
        },
        addVisitedView(view: RouteLocationNormalizedLoaded) {
            if (this.visitedViews.some(v => v.path === view.path)) return
            this.visitedViews.push(Object.assign({}, view))
        },
        addCachedView(view: RouteLocationNormalizedLoaded) {
            if (this.cachedViews.includes(view.name as string)) return
            if (!view.meta.noCache) {
                this.cachedViews.push(view.name as string)
            }
        },
        delView(view: RouteLocationNormalizedLoaded): Promise<{ visitedViews: RouteLocationNormalizedLoaded[]; cachedViews: string[] }> {
            return new Promise(resolve => {
                this.delVisitedView(view)
                this.delCachedView(view)
                resolve({
                    visitedViews: [...this.visitedViews],
                    cachedViews: [...this.cachedViews],
                })
            })
        },
        delVisitedView(view: RouteLocationNormalizedLoaded): Promise<RouteLocationNormalizedLoaded[]> {
            return new Promise(resolve => {
                for (const [i, v] of this.visitedViews.entries()) {
                    if (v.path === view.path) {
                        this.visitedViews.splice(i, 1)
                        break
                    }
                }
                resolve([...this.visitedViews])
            })
        },
        delCachedView(view: RouteLocationNormalizedLoaded): Promise<string[]> {
            return new Promise(resolve => {
                const index = this.cachedViews.indexOf(view.name as string)
                index > -1 && this.cachedViews.splice(index, 1)
                resolve([...this.cachedViews])
            })
        },
        delOthersViews(view: RouteLocationNormalizedLoaded): Promise<{ visitedViews: RouteLocationNormalizedLoaded[]; cachedViews: string[] }> {
            return new Promise(resolve => {
                this.delOthersVisitedViews(view)
                this.delOthersCachedViews(view)
                resolve({
                    visitedViews: [...this.visitedViews],
                    cachedViews: [...this.cachedViews],
                })
            })
        },
        delOthersVisitedViews(view: RouteLocationNormalizedLoaded): Promise<RouteLocationNormalizedLoaded[]> {
            return new Promise(resolve => {
                // 排除首页和当前页
                this.visitedViews = this.visitedViews.filter(v => {
                    return v.name == 'Index' || v.path === view.path
                })
                resolve([...this.visitedViews])
            })
        },
        delOthersCachedViews(view: RouteLocationNormalizedLoaded): Promise<string[]> {
            return new Promise(resolve => {
                const index = this.cachedViews.indexOf(view.name as string)
                if (index > -1) {
                    this.cachedViews = this.cachedViews.slice(index, index + 1)
                } else {
                    this.cachedViews = []
                }
                resolve([...this.cachedViews])
            })
        },
        delAllViews(): Promise<{ visitedViews: RouteLocationNormalizedLoaded[]; cachedViews: string[] }> {
            return new Promise(resolve => {
                this.delAllVisitedViews()
                this.delAllCachedViews()
                resolve({
                    visitedViews: [...this.visitedViews],
                    cachedViews: [...this.cachedViews],
                })
            })
        },
        delAllVisitedViews(): Promise<RouteLocationNormalizedLoaded[]> {
            return new Promise(resolve => {
                // 只保留首页
                this.visitedViews = this.visitedViews.filter(tag => tag.name == 'Index')
                resolve([...this.visitedViews])
            })
        },
        delAllCachedViews(): Promise<string[]> {
            return new Promise(resolve => {
                this.cachedViews = []
                resolve([...this.cachedViews])
            })
        },
        updateVisitedView(view: RouteLocationNormalizedLoaded) {
            for (let v of this.visitedViews) {
                if (v.path === view.path) {
                    v = Object.assign(v, view)
                    break
                }
            }
        },
    },
})

export default useTagsViewStore
