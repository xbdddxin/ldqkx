import Cookies from 'js-cookie'

interface AppState {
    // 黑夜模式
    darkMode?: String
    // 是否移动设备
    isMobile: boolean
    // 侧边栏
    sidebar: {
        // 侧边栏是否展开
        isCollapse: boolean
        // 侧边栏是否隐藏（不是折叠侧边栏，是直接消失）
        hide: boolean
    }
}

const useAppStore = defineStore('app', {
    state: (): AppState => ({
        isMobile: false,
        sidebar: {
            isCollapse: Cookies.get('sidebarStatus') ? Cookies.get('sidebarStatus') == '1' : false,
            hide: false,
        },
    }),
    actions: {
        toggleSideBar() {
            if (this.sidebar.hide) {
                return false
            }
            this.sidebar.isCollapse = !this.sidebar.isCollapse
            if (this.sidebar.isCollapse) {
                Cookies.set('sidebarStatus', '1')
            } else {
                Cookies.set('sidebarStatus', '0')
            }
        },
        closeSideBar() {
            Cookies.set('sidebarStatus', '1')
            this.sidebar.isCollapse = true
        },
        toggleMobile(isMobile: boolean) {
            this.isMobile = isMobile
        },
        toggleSideBarHide(status: boolean) {
            this.sidebar.hide = status
        },
    },
})

export default useAppStore
