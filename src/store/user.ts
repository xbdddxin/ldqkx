import { ElMessage } from 'element-plus'
import { userLogin, userInfo, addUser } from '@/api/system/user'
import type { LoginParams, UserModel } from '@/api/system/model/userModel'
import { getToken, setToken, removeToken } from '@/utils/auth'
import Profile from '@/assets/images/profile.png'

interface UserState {
    userInfo: Nullable<UserModel>
    token?: string
}

const useUserStore = defineStore('user', {
    state: (): UserState => ({
        // 用户信息
        userInfo: null,
        // 验证用token
        token: getToken(),
    }),
    actions: {
        // 登录
        async login(params: LoginParams) {
            try {
                const data = await userLogin(params)
                const { token } = data

                // save token
                setToken(token)

                ElMessage({ message: '登录成功', type: 'success', center: true })
                return Promise.resolve()

                // return this.getUserInfo()
            } catch (error) {
                return Promise.reject(error)
            }
        },
        // 获取用户信息
        getUserInfo(): Promise<UserModel | null> {
            return new Promise((resolve, reject) => {
                userInfo()
                    .then(res => {
                        // 如果用户没有设置头像则使用默认头像
                        if (!res.user_info.header_img) {
                            res.user_info.header_img = Profile
                        }

                        this.userInfo = res.user_info
                        resolve(this.userInfo)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        // 登出
        logout() {
            return new Promise((resolve, reject) => {
                removeToken()
                location.href = '/login'
            })
        },
        // 注册
        regist(params: LoginParams) {
            return new Promise((resolve, reject) => {
                addUser(params)
                    .then(res => {
                        resolve(res)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
    },
})

export default useUserStore
