/**
 * 判断路径开头是否有斜杠 /
 * @param {string} path
 * @returns {Boolean}
 */
export function isStartWidthSlash(path: string): boolean {
    return path[0] === '/'
}
