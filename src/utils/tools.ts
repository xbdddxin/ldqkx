// @ts-expect-error moduleResolution:nodenext issue 54523
import { intersectionWith, isEqual, mergeWith, unionWith } from 'lodash-es'
import { useWindowSize } from '@vueuse/core'
import { dayjs } from 'element-plus'

/**
 * Recursively merge two objects.
 * 递归合并两个对象。
 *
 * @param source The source object to merge from. 要合并的源对象。
 * @param target The target object to merge into. 目标对象，合并后结果存放于此。
 * @param mergeArrays How to merge arrays. Default is "replace".
 *        如何合并数组。默认为replace。
 *        - "union": Union the arrays. 对数组执行并集操作。
 *        - "intersection": Intersect the arrays. 对数组执行交集操作。
 *        - "concat": Concatenate the arrays. 连接数组。
 *        - "replace": Replace the source array with the target array. 用目标数组替换源数组。
 * @returns The merged object. 合并后的对象。
 */
export function deepMerge<T extends object | null | undefined, U extends object | null | undefined>(
    source: T,
    target: U,
    mergeArrays: 'union' | 'intersection' | 'concat' | 'replace' = 'replace'
): T & U {
    if (!target) {
        return source as T & U
    }
    if (!source) {
        return target as T & U
    }
    return mergeWith({}, source, target, (sourceValue, targetValue) => {
        if (Array.isArray(targetValue) && Array.isArray(sourceValue)) {
            switch (mergeArrays) {
                case 'union':
                    return unionWith(sourceValue, targetValue, isEqual)
                case 'intersection':
                    return intersectionWith(sourceValue, targetValue, isEqual)
                case 'concat':
                    return sourceValue.concat(targetValue)
                case 'replace':
                    return targetValue
                default:
                    throw new Error(`Unknown merge array strategy: ${mergeArrays as string}`)
            }
        }
        if (targetValue != null && sourceValue != null) {
            return deepMerge(sourceValue, targetValue, mergeArrays)
        }
        return undefined
    })
}

// 判断是否为移动端
export function isMobileDevice() {
    const { width } = useWindowSize()
    const WIDTH = 992
    if (width.value - 1 < WIDTH) {
        return true
    } else {
        return false
    }
}

// 格式化时间
export function timeFormatter(time: string | undefined) {
    if (time) {
        return dayjs(time).format('YYYY-MM-DD hh:mm:ss')
    } else {
        dayjs().format('YYYY-MM-DD hh:mm:ss')
    }
}
