import axios from 'axios'
import type { AxiosRequestConfig, AxiosInstance, AxiosResponse, AxiosError, InternalAxiosRequestConfig } from 'axios'
import type { Result, RequestOptions, CreateAxiosOptions } from '@/types/axios'
import { ResultEnum, ContentTypeEnum } from '@/constants/httpEnum'
// @ts-expect-error moduleResolution:nodenext issue 54523
import { cloneDeep } from 'lodash-es'
import { ElMessage } from 'element-plus'
import { getToken } from '@/utils/auth'
import { deepMerge } from '@/utils/tools'

class Axios {
    private axiosInstance: AxiosInstance
    private readonly options: CreateAxiosOptions

    constructor(options: CreateAxiosOptions) {
        this.options = options
        this.axiosInstance = axios.create(options)
        this.setupInterceptors()
    }

    get<T = any>(config: AxiosRequestConfig, options?: RequestOptions): Promise<T> {
        return this.request({ ...config, method: 'GET' }, options)
    }

    post<T = any>(config: AxiosRequestConfig, options?: RequestOptions): Promise<T> {
        return this.request({ ...config, method: 'POST' }, options)
    }

    put<T = any>(config: AxiosRequestConfig, options?: RequestOptions): Promise<T> {
        return this.request({ ...config, method: 'PUT' }, options)
    }

    delete<T = any>(config: AxiosRequestConfig, options?: RequestOptions): Promise<T> {
        return this.request({ ...config, method: 'DELETE' }, options)
    }

    request<T = any>(config: CreateAxiosOptions, options?: RequestOptions): Promise<T> {
        const conf: CreateAxiosOptions = cloneDeep(config)
        const { requestOptions } = this.options
        const opt: RequestOptions = Object.assign({}, requestOptions, options)

        conf.requestOptions = opt

        return new Promise((resolve, reject) => {
            this.axiosInstance
                .request<any, AxiosResponse<Result>>(conf)
                .then((res: AxiosResponse<Result>) => {
                    resolve(res as unknown as Promise<T>)
                })
                .catch((e: Error | AxiosError) => {
                    if (axios.isAxiosError(e)) {
                        console.log(e)
                    }
                    reject(e)
                })
        })
    }

    private setupInterceptors() {
        // 请求拦截器
        this.axiosInstance.interceptors.request.use(
            (config: InternalAxiosRequestConfig) => {
                const token = getToken()
                if (token && (config as Recordable)?.requestOptions?.withToken !== false) {
                    // jwt token
                    ;(config as Recordable).headers.Authorization = this.options.authenticationScheme ? `${this.options.authenticationScheme} ${token}` : token
                }
                return config
            },
            error => {
                console.log(error)
                Promise.reject(error)
            }
        )
        // 响应拦截器
        this.axiosInstance.interceptors.response.use(
            (res: AxiosResponse<any>) => {
                const { isTransformResponse, isNativeResponse } = this.options.requestOptions as unknown as RequestOptions
                // 是否需要原生的响应(不做任何处理直接返回response)
                if (isNativeResponse) {
                    return res
                }

                // 是否需要转换响应(转换的话只返回result部分,code和msg去掉了)
                if (!isTransformResponse) {
                    return res.data
                }

                const { data } = res
                if (!data) {
                    ElMessage({
                        message: '[HTTP] Request has no return value',
                        type: 'error',
                    })
                }

                //  这里 code，result，msg 为 后台统一的字段，需要在 types.ts内修改为项目自己的接口返回格式
                const { code, result = data.data, msg } = data

                // 这里逻辑可以根据项目进行修改
                const isSuccess = data && Reflect.has(data, 'code') && code === ResultEnum.SUCCESS
                if (isSuccess) {
                    // 二进制数据则直接返回
                    if (res.request.responseType === 'blob' || res.request.responseType === 'arraybuffer') {
                        return res.data
                    }

                    return Promise.resolve(result)
                } else {
                    ElMessage({
                        message: msg,
                        type: 'error',
                        duration: 5 * 1000,
                    })
                    return Promise.reject(msg)
                }
            },
            error => {
                let { message } = error
                if (message == 'Network Error') {
                    message = '后端接口连接异常'
                } else if (message.includes('timeout')) {
                    message = '系统接口请求超时'
                } else if (message.includes('Request failed with status code')) {
                    message = '系统接口' + message.substr(message.length - 3) + '异常'
                } else if (message.includes('canceled')) {
                    return
                }

                ElMessage({
                    message,
                    type: 'error',
                    duration: 5 * 1000,
                })

                return Promise.reject(error)
            }
        )
    }
}

function createAxios(opt?: Partial<CreateAxiosOptions>) {
    return new Axios(
        // 深度合并
        deepMerge(
            {
                // See https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#authentication_schemes
                // authenticationScheme: 'Bearer',
                // 发送请求时自动在token前加上的字符串
                authenticationScheme: '',
                timeout: 10 * 1000,
                // 基础接口地址
                baseURL: import.meta.env.VITE_APP_BASE_API,
                headers: { 'Content-Type': ContentTypeEnum.JSON },
                // 如果是form-data格式
                // headers: { 'Content-Type': ContentTypeEnum.FORM_URLENCODED },
                // 配置项，下面的选项都可以在独立的接口请求中覆盖
                requestOptions: {
                    // 是否携带token
                    withToken: true,
                    // 是否需要原生的响应(不做任何处理直接返回response)
                    isNativeResponse: false,
                    // 是否需要转换响应(转换的话只返回result部分,code和msg去掉了)
                    isTransformResponse: true,
                },
            },
            opt || {}
        )
    )
}
export const defHttp = createAxios()
