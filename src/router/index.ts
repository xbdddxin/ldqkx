import type { RouteRecordRaw } from 'vue-router'
import { createRouter, createWebHistory } from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { ElMessage } from 'element-plus'
import { getToken } from '@/utils/auth'
import useUserStore from '@/store/user'
import usePermissionStore from '@/store/permission'

import DefaultLayout from '@/layouts/default/index.vue'

// 首页
export const HomePage: RouteRecordRaw = {
    path: '',
    component: DefaultLayout,
    redirect: '/index',
    children: [
        {
            path: '/index',
            name: 'Index',
            component: () => import('@/views/index.vue'),
            meta: { title: '首页', icon: 'home-filled' },
        },
    ],
}

// 公共路由
export const publicRoutes: RouteRecordRaw[] = [
    HomePage,
    {
        path: '/redirect',
        component: DefaultLayout,
        children: [
            {
                path: ':path(.*)',
                component: () => import('@/views/common/Redirect.vue'),
            },
        ],
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/Login.vue'),
    },
    {
        path: '/user',
        component: DefaultLayout,
        redirect: 'noredirect',
        children: [
            {
                path: 'center',
                component: () => import('@/views/system/user/center/index.vue'),
                name: 'Profile',
                meta: { title: '个人中心', icon: 'user' },
            },
        ],
    },
    {
        // 如果域名后面什么都不写或者没有和已经定义好的路由匹配的话
        path: '/:pathMatch(.*)*',
        component: () => import('@/views/common/404.vue'),
    },
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: publicRoutes,
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { top: 0 }
        }
    },
})

NProgress.configure({ showSpinner: false })
// 菜单白名单（不需要权限即可访问）
const whiteList = ['/login']
// 路由前置守卫
router.beforeEach((to, from, next) => {
    NProgress.start()
    if (getToken()) {
        // to.meta.title && useSettingsStore().setTitle(to.meta.title)
        /* has token*/
        if (to.path === '/login') {
            next({ path: '/' })
            NProgress.done()
        } else {
            const userStore = useUserStore()
            // 判断当前用户是否已拉取完user_info信息
            if (!userStore.userInfo?.role.role_value) {
                userStore
                    .getUserInfo()
                    .then(() => {
                        usePermissionStore()
                            .buildRoutes()
                            .then(accessRoutes => {
                                // 根据用户权限生成可访问的路由表
                                accessRoutes?.forEach(route => {
                                    router.addRoute(route as unknown as RouteRecordRaw) // 动态添加可访问路由表
                                })
                                next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
                            })
                    })
                    .catch(err => {
                        userStore.logout().then(() => {
                            ElMessage.error(err)
                            next({ path: '/' })
                        })
                    })
            } else {
                next()
            }
        }
    } else {
        // 没有token
        if (whiteList.indexOf(to.path) !== -1) {
            // 在免登录白名单，直接进入
            next()
        } else {
            next(`/login?redirect=${to.fullPath}`) // 否则全部重定向到登录页
            NProgress.done()
        }
    }
})

// 路由后置守卫
router.afterEach(() => {
    NProgress.done()
})

export default router
