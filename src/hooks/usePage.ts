import type { FormInstance, FormRules } from 'element-plus'
import type { PageProps } from '@/components/BasicPage/index'

interface SearchFormParams extends Recordable {
    page?: number
    limit?: number
}

export function usePage(options: PageProps) {
    const { proxy } = getCurrentInstance()!
    const list = ref() //表格数据list
    const total = ref(0) //表格数据总数
    const loading = ref(true) //是否加载数据中
    const form = ref<DynamicObject & { id?: number }>({}) //对话框表单参数
    const searchForm = ref<SearchFormParams>({}) //查询表单参数
    const dialogVisible = ref(false) //对话框是否可见
    const dialogTitle = ref('') //对话框标题
    const showSearch = ref(options.showSearch) //是否展示条件查询
    const rules = ref<FormRules>() //对话框表单验证规则
    const queryFormRef = ref<FormInstance>() //查询表单dom
    const dialogFormRef = ref<FormInstance>() //对话框表单dom

    const setFieldsValues = ref<{ props: string; setFieldsValue: (field: any) => any }[]>([]) //监听 formSchema 的 setFieldsValue 函数

    //对话框表单参数
    if (options.form && options.form.length > 0) {
        const paramList = options.form.map(s => {
            const a: Recordable = {}
            a[s.props] = undefined

            // 如果有设置 setFieldsValue 则准备监听该属性
            if (s.setFieldsValue) {
                setFieldsValues.value.push({ props: s.props, setFieldsValue: s.setFieldsValue })
            }

            return a
        })
        form.value = Object.assign({}, ...paramList)
    }

    //查询表单参数
    if (options.searchFrom && options.searchFrom.length > 0) {
        const paramList = options.searchFrom.map(s => {
            const a: SearchFormParams = {}
            a[s.props] = undefined
            return a
        })
        searchForm.value = Object.assign({ page: 1, limit: 10 }, ...paramList)
    }

    // 对话框表单验证规则
    if (options.form && options.form.length > 0) {
        const ruleList = options.form.map(s => {
            const a: Recordable = {}
            a[s.props] = [s.rule]
            return a
        })
        rules.value = Object.assign({}, ...ruleList)
    }

    // 获取表格数据
    const getList = () => {
        loading.value = true
        options.api.List!(searchForm.value).then(data => {
            list.value = data.list
            total.value = data.total
            loading.value = false
        })
    }

    // 查询
    const handleSearch = () => {
        searchForm.value.page = 1
        getList()
    }

    // 重置查询条件
    function resetQuery() {
        queryFormRef.value?.resetFields()
        handleSearch()
    }

    // 添加
    const handleAdd = () => {
        reset()
        dialogVisible.value = true
        dialogTitle.value = `添加${options.title}`
    }

    // 修改
    const handleUpdate = (row: any) => {
        reset()
        options.api.Get!(row.id).then((res: any) => {
            form.value = res
            dialogVisible.value = true
            dialogTitle.value = `修改${options.title}`
        })
    }

    // 删除
    const handleDelete = (row: any) => {
        const columnName = options.columns.find(c => c.isName == true)
        proxy?.$msgbox
            .confirm(`是否确认删除${options.title}` + (columnName ? row[columnName.props] : '') + '"？')
            .then(() => {
                options.api.Delete!(row.id).then(() => {
                    getList()
                    proxy?.$message.success('删除成功')
                })
            })
            .catch(() => {})
    }

    // 对话框确定按钮
    const submitForm = () => {
        dialogFormRef.value?.validate(valid => {
            if (valid && form.value) {
                if (form.value.id != undefined) {
                    options.api.Update!(form.value).then(() => {
                        proxy?.$message.success('修改成功')
                        dialogVisible.value = false
                        getList()
                    })
                } else {
                    options.api.Add!(form.value).then(() => {
                        proxy?.$message.success('新增成功')
                        dialogVisible.value = false
                        getList()
                    })
                }
            }
        })
    }

    // 对话框取消按钮
    const cancel = () => {
        dialogVisible.value = false
        reset()
    }

    // 重置对话框中的表单
    const reset = () => {
        form.value = {}
        dialogFormRef.value?.resetFields()
    }

    watchEffect(() => {
        // 监听有设置 setFieldsValue 的属性，属性值一旦发生变化就立即调用 setFieldsValue
        setFieldsValues.value.forEach(s => {
            form.value[s.props] = s.setFieldsValue(form.value[s.props]) || ''
        })
    })

    return {
        list,
        total,
        loading,
        form,
        searchForm,
        dialogVisible,
        dialogTitle,
        showSearch,
        rules,
        queryFormRef,
        dialogFormRef,
        getList,
        handleSearch,
        resetQuery,
        handleAdd,
        handleUpdate,
        handleDelete,
        submitForm,
        cancel,
    }
}
