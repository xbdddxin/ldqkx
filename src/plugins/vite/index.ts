import vue from '@vitejs/plugin-vue'
import createSetupExtend from './setup-extend'
import createAutoImport from './auto-import'

interface Options {
    isBuild: boolean
}

async function createPlugins({ isBuild }: Options) {
    const vitePlugins = [vue()]
    vitePlugins.push(createSetupExtend())
    vitePlugins.push(createAutoImport())
    // isBuild && vitePlugins.push(...createCompression(isBuild))
    return vitePlugins
}

export { createPlugins }
