import autoImport from 'unplugin-auto-import/vite'

export default function createAutoImport() {
    return autoImport({
        imports: ['vue', 'vue-router', 'pinia'],
        // 在ts项目中需要自动生成类型声明文件防止类型报错
        dts: 'src/types/auto-import.d.ts',
    })
}
