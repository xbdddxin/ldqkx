// Make the vue script setup syntax support the name attribute
import vueSetupExtend from 'vite-plugin-vue-setup-extend'

export default function createSetupExtend() {
    return vueSetupExtend()
}
