import type { FormItemRule } from 'element-plus'
import type { BasicModel, GetListResultModel } from '@/api/model/baseModel'

export interface PageApi {
    List?: (...arg: any) => Promise<GetListResultModel<any>>
    Get?: (...arg: any) => Promise<any>
    Add?: (...arg: any) => Promise<any>
    Update?: (...arg: any) => Promise<any>
    Delete?: (...arg: any) => Promise<any>
}

export interface BasicColumn {
    label: string
    props: string
    // 是否是该记录的名字（如：'是否确认删除 row.role_name' ，role_name 的 isName 为 true 才能知道这里是要用 role_name）
    isName?: boolean
    formatter?(row: BasicModel): string | undefined
}

export interface formSchema {
    label: string
    props: string
    rule?: FormItemRule
    // 设置表单字段值（一般用来对表单字段值做附加处理，比如要最终的字段值转换为 number 类型，可以用此函数设置）
    setFieldsValue?(field?: any): any
}

export interface PageProps {
    // 页面标题
    title: string
    // 列表、增删改查的接口
    api: PageApi
    // 表格列
    columns: BasicColumn[]
    // 对话框中增改的form
    form: formSchema[]
    // 是否展示查询
    showSearch?: boolean
    // 查询的form
    searchFrom?: formSchema[]
}
