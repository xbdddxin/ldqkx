import { defHttp } from '@/utils/request'
import { MenuParams, MenuModel } from './model/menuModel'

enum Api {
    List = '/menu/getMenuList',
    Get = '/menu/getMenuById',
    Add = '/menu/addMenu',
    Update = '/menu/updateMenu',
    Delete = '/menu/deleteMenu',
}

export const getMenuList = () => {
    return defHttp.get<MenuModel[]>({ url: Api.List })
}

export const getMenu = (id: number) => {
    return defHttp.get<MenuModel>({ url: Api.Get, params: { id } })
}

export const addMenu = (data: MenuParams) => {
    return defHttp.post<MenuModel[]>({ url: Api.Add, data })
}

export const updateMenu = (data: MenuParams) => {
    return defHttp.post<MenuModel>({ url: Api.Update, data })
}

export const deleteMenu = (id: number) => {
    return defHttp.delete({ url: Api.Delete + '/' + id })
}
