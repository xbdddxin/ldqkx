import { defHttp } from '@/utils/request'
import type { BasicPageParams, GetListResultModel } from '../model/baseModel'
import type { RoleModel, RoleParams } from './model/roleModel'

enum Api {
    List = '/role/list',
    Get = '/role/detail',
    Add = '/role/create',
    Update = '/role/update',
    Delete = '/role',
}

export const getRoleList = (params: BasicPageParams) => {
    return defHttp.get<GetListResultModel<RoleModel>>({ url: Api.List, params })
}

export const getRole = (id: number) => {
    return defHttp.get<RoleModel>({ url: Api.Get + '/' + id })
}

export const addRole = (data: RoleParams) => {
    return defHttp.post<GetListResultModel<RoleModel>>({ url: Api.Add, data })
}

export const updateRole = (data: RoleParams) => {
    return defHttp.post<RoleModel>({ url: Api.Update, data })
}

export const deleteRole = (id: number) => {
    return defHttp.delete({ url: Api.Delete + '/' + id })
}
