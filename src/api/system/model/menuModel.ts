import type { RouteMeta } from 'vue-router'
import type { BasicModel } from '@/api/model/baseModel'

// 路由 Model
export interface RouteItem {
    id: number
    path: string
    component: any
    name?: string
    redirect?: string
    meta?: RouteMeta
    children?: RouteItem[]
}

// 菜单 Model
export interface MenuModel extends RouteItem, BasicModel {}

// 添加/修改菜单参数
export type MenuParams = Partial<MenuModel>
