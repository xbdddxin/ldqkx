import type { RoleModel } from './roleModel'
import type { BasicModel } from '@/api/model/baseModel'

// 用户Model
export interface UserModel extends BasicModel {
    id: number
    username: string
    display_name: string
    custom_group: string
    remark: string
    header_img: string
    role_id: number
    role: RoleModel
}

// 添加/修改用户参数
export interface UserParams extends Partial<Omit<UserModel, 'role'>> {
    password?: string
}

// 登录注册结果
export interface LoginRegistResultModel {
    username: string
    token: string
}

// 登录注册参数
export type LoginParams = {
    username: string
    password: string
}

// 获取用户信息结果
export interface UserInfoResuleModel {
    has_password: true
    user_info: UserModel
}
