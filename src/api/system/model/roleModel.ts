import type { BasicModel } from '@/api/model/baseModel'

// 角色Model
export interface RoleModel extends BasicModel {
    id: number
    role_name: string
    role_value: number
    description: string
    sort: number
}

// 添加/修改角色参数
export type RoleParams = Partial<RoleModel>
