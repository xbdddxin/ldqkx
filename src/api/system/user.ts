import { defHttp } from '@/utils/request'
import type { BasicPageParams, GetListResultModel } from '@/api/model/baseModel'
import type { UserModel, UserInfoResuleModel, LoginParams, LoginRegistResultModel, UserParams } from './model/userModel'

enum Api {
    List = '/user/list',
    Get = '/user/detail',
    Add = '/user/register',
    Update = '/user/userInfo/update',
    Delete = '/user',
    Login = '/user/login',
    Info = '/user/userInfo',
}

export const userLogin = (data: LoginParams) => {
    return defHttp.post<LoginRegistResultModel>({ url: Api.Login, data })
}

export const userInfo = () => {
    return defHttp.get<UserInfoResuleModel>({ url: Api.Info })
}

export const getUserList = (params: BasicPageParams) => {
    return defHttp.get<GetListResultModel<UserModel>>({ url: Api.List, params })
}

export const getUser = (id: number) => {
    return defHttp.get<UserModel>({ url: Api.Get + '/' + id })
}

export const addUser = (data: UserParams) => {
    return defHttp.post<LoginRegistResultModel>({ url: Api.Add, data })
}

export const updateUser = (data: UserParams) => {
    return defHttp.post<UserModel>({ url: Api.Update, data })
}

export const deleteUser = (id: number) => {
    return defHttp.delete({ url: Api.Delete + '/' + id })
}
