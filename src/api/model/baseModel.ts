export interface BasicModel {
    created_at: string
    updated_at: string
}

// 基本请求参数
export interface BasicPageParams {
    page: number
    limit: number
}

// 获取列表结果Model
export interface GetListResultModel<T> {
    list: T[]
    total: number
}
