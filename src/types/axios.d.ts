import type { AxiosRequestConfig } from 'axios'

// 接口response类型
export interface Result<T = any> {
    code: number
    msg: string
    data: T
}

// 请求配置对象
export interface RequestOptions {
    // 请求头是否要带token
    withToken?: boolean
    // 是否需要原生的响应(不做任何处理直接返回response)
    isNativeResponse?: boolean
    // 是否需要转换响应(转换的话只返回result部分,code和msg去掉了)
    isTransformResponse?: boolean
}

// create axios 配置对象
export interface CreateAxiosOptions extends AxiosRequestConfig {
    requestOptions?: RequestOptions
    // 身份验证方案,常用的是 Bearer,详见 https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#authentication_schemes
    authenticationScheme?: string
}
