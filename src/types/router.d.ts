export {}

// 定义 vue-router 中 meta 的类型
declare module 'vue-router' {
    interface RouteMeta extends Record<string | number | symbol, unknown> {
        // 标题
        title?: string
        // 顺序
        orderNo?: number
        // icon on tab
        icon?: string
        // 是否缓存（keep-alive）
        noCache?: boolean
    }
}
