declare type Nullable<T> = T | null
declare type Recordable<T = any> = Record<string, T>
declare type DynamicObject = { [key: string]: any }
