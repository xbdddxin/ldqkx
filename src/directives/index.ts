/**
 * 注册自定义指令
 */
import type { App } from 'vue'
import hasRole from './hasRole'

export function rigistDirective(app: App) {
    app.directive('hasRole', hasRole)
}
