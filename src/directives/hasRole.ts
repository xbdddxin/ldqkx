/**
 * v-hasRole 角色权限处理
 */
import type { App, Directive, DirectiveBinding } from 'vue'
import useUserStore from '@/store/user'

const directive: Directive = {
    mounted(el: Element, binding: DirectiveBinding<Array<number>>, vnode: VNode) {
        const { value } = binding
        const super_admin = import.meta.env.VITE_SUPER_ADMIN
        const role = useUserStore().userInfo?.role.role_value

        if (value && role) {
            const hasRole = super_admin == role || value.includes(role)

            if (!hasRole) {
                el.parentNode && el.parentNode.removeChild(el)
            }
        } else {
            throw new Error(`请设置角色权限标签值`)
        }
    },
}

export default directive
