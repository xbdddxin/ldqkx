/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
    root: true,
    extends: [
        // https://eslint.vuejs.org/rules/
        'plugin:vue/vue3-essential',
        // https://eslint.org/docs/latest/rules/
        'eslint:recommended',
        // https://typescript-eslint.io/rules/
        '@vue/eslint-config-typescript',
        // https://github.com/vuejs/eslint-config-prettier#use-separate-commands-for-linting-and-formatting
        '@vue/eslint-config-prettier/skip-formatting',
    ],
    parserOptions: {
        ecmaVersion: 'latest',
    },
    rules: {
        'vue/multi-word-component-names': 'off', // 关闭组件命名规则
        semi: [2, 'never'], // 禁止分号
        quotes: [
            2,
            'single', // 单引号
            {
                avoidEscape: true,
                allowTemplateLiterals: true,
            },
        ],
        // indent: [2, 4], // 4个空格缩进
        'jsx-quotes': [2, 'prefer-single'], // JSX使用单引号
        'no-const-assign': 2, // 禁止修改 const 声明的变量
        'no-dupe-keys': 2, // 禁止对象中出现重复的键
    },
}
